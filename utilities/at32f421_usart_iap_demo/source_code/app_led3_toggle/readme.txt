/**
  **************************************************************************
  * @file     readme.txt
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, shows the app code
  operating flow for at32f4xx series. led3 on the at-start board is twinkling
  when app code is running. for more detailed information. please refer to the
  application note document AN0001.
