/**
  **************************************************************************
  * @file     readme.txt
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, shows the bootloader
  operating flow for at32f4xx series. led2 on the at-start board is twinkling
  when iap bootloader is running. for more detailed information. please refer 
  to the application note document AN0001.