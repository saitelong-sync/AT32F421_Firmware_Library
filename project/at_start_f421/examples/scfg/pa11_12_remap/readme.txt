/**
  **************************************************************************
  * @file     readme.txt
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, tmr1 ch4 output pwm
  waveform on small packages (20-pin) with pin pa9.
