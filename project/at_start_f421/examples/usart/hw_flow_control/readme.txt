/**
  **************************************************************************
  * @file     readme.txt 
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, provides a description
  of how to use the usart hardware flow control function, the demo communicate with
  the hyperterminal.
  
  set-up 
  - connect usart2 tx pin(pa2)/rx pin(pa3)/cts pin(pa0)/rts pin(pa1)

