/**
  **************************************************************************
  * @file     readme.txt 
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, have two project
  targets: transmit and receive. the demo provides a basic communication with
  irda mode. if received data right ,the led4 will turn on.
  
  set-up
  - use usart2 tx pin (pa2) and rx pin (pa3)
