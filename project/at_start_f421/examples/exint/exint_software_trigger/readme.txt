/**
  **************************************************************************
  * @file     readme.txt 
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, shows how to use
  software trigger exint.
  software trigger will be generate in tmr1 overflow interrupt.
  led2 toggle means tmr1 overflow interrupt respond.
  led3 and led4 toggle means exint line 4  interrupt respond.
