/**
  **************************************************************************
  * @file     readme.txt
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, pa8 output 9.88 khz
  waveforms, pa8 toggled each pa0, pa1, pa2 edge.
  pin connection:
  - pa0    <--->    pb0
  - pa1    <--->    pa6
  - pa2    <--->    pa7 
