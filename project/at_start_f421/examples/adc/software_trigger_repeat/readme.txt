/**
  **************************************************************************
  * @file     readme.txt
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, shows how to use
  the software trigger source trigger adc.
  the convert data as follow:
  - adc1_ordinary_valuetab[n][0] ---> adc1_channel_4
  - adc1_ordinary_valuetab[n][1] ---> adc1_channel_5
  - adc1_ordinary_valuetab[n][2] ---> adc1_channel_6
