/**
  **************************************************************************
  * @file     readme.txt
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this example shows how to configure cmp to wakeup deep sleep mode, if wakeup
  from deep sleep mode, usart1 will print some messages(pa9). the cmp_out(pa6) will
  output the result of compare too.
