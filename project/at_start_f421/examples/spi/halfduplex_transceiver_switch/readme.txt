/**
  **************************************************************************
  * @file     readme.txt
  * @version  v2.0.1
  * @date     2021-11-26
  * @brief    readme
  **************************************************************************
  */

  this demo is based on the at-start board, in this demo, shows how to use
  halfduplex mode transfer data by interrupt mode. use single line switch
  realize data send and receive.  
  the pins connection as follow:
  - spi2 slaver        spi1 master
  - pb13     <--->     pa5
  - pb14     <--->     pa7
